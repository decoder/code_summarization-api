# -*- coding: utf-8 -*-

import os 
# Disables the warning "Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA", doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import logging
import time
import warnings
warnings.filterwarnings('ignore', category=FutureWarning)
import tensorflow as tf
tf.get_logger().setLevel('ERROR')
import json
import requests
from flask import abort

# Remove requests warnings
requests.packages.urllib3.disable_warnings()
# Remove KMP_AFFINITY logs
os.environ['KMP_WARNINGS'] = 'off'
# Load the logger
logger = logging.getLogger("DECODER_API.core.model")

from .imports.transformer_modules import Transformer, Encoder, Decoder, MultiHeadAttention
from .imports.training_utils import  seq2text, summarize  
from .imports.training_utils import feature_extraction_inference
from .imports.preprocessing_java import tokenization_java
from .imports.preprocessing_c import tokenization_c
from .code_utils import split_methods_java, split_methods_c, split_methods_cpp, add_prediction_comments, filter_ast_dicts
from swagger_server.commons.constants import DEFAULT_RESOURCES_PATH, PKM_API_RAW_PATH, PKM_API_JAVA_SOURCE_PATH, PKM_API_C_SOURCE_PATH, PKM_API_CPP_SOURCE_PATH, JAVA_TRANSFORMER, TOKEN2IDX_JAVA, IDX2SUMMARY_JAVA, C_TRANSFORMER, TOKEN2IDX_C, IDX2SUMMARY_C, MAX_LENGTH_CODE, MAX_LENGTH_SUMMARY


def check_reply(reply, method):
    if (reply.ok) or (method == 'get' and reply.status_code == 404):
        try:
            return reply.json()
        except:
            pass
    else:
        # If response code is not ok (200) raise the error
        reply.raise_for_status()

class Model():
    """
    
    """
    def __init__(self):

        """
        Creates a Model instance, loading the trained Keras model.

        Parameters
        ----------
        None
        """
        self.base_path = DEFAULT_RESOURCES_PATH
        self.raw_pkm_url = PKM_API_RAW_PATH
        self.java_source_pkm_url = PKM_API_JAVA_SOURCE_PATH
        self.c_source_pkm_url = PKM_API_C_SOURCE_PATH
        self.cpp_source_pkm_url  = PKM_API_CPP_SOURCE_PATH

        # Java models and data
        self.java_transformer_name = JAVA_TRANSFORMER
        self.java_token_dict_name = TOKEN2IDX_JAVA
        self.java_summary_dict_name = IDX2SUMMARY_JAVA

        try:
            with open(self.base_path + self.java_token_dict_name) as json_file:
                self.java_token2idx = json.load(json_file)
        except Exception as err:
            logger.exception('Could not open the file %s' %self.java_token_dict_name)
            raise

        try:
            with open(self.base_path + self.java_summary_dict_name) as json_file:
                self.java_idx2summary = json.load(json_file)
        except Exception as err:
            logger.exception('Could not open the file %s' %self.java_summary_dict_name)
            raise

        # Fix - Parse keys as ints
        self.java_token2idx = {key: int(value) for key, value in self.java_token2idx.items()}
        self.java_idx2token = {value: key for key, value in self.java_token2idx.items()}
        self.java_idx2summary = {int(key): value for key, value in self.java_idx2summary.items()}
        self.java_summary2idx = {w: i for i, w in self.java_idx2summary.items()}        

        try:
            self.java_transformer_model = tf.keras.models.load_model(self.base_path + self.java_transformer_name,
                                                                     custom_objects={'Transformer':Transformer, 'Encoder': Encoder, 'Decoder': Decoder, 'MultiHeadAttention': MultiHeadAttention},
                                                                     compile=False)
        except Exception as err:
            logger.exception('Could not load the model %s' %self.java_transformer_name)
            raise

        # C, C++ models and data
        self.c_transformer_name = C_TRANSFORMER
        self.c_token_dict_name = TOKEN2IDX_C
        self.c_summary_dict_name = IDX2SUMMARY_C

        try:
            with open(self.base_path + self.c_token_dict_name) as json_file:
                self.c_token2idx = json.load(json_file)
        except Exception as err:
            logger.exception('Could not open the file %s' %self.c_token_dict_name)
            raise

        try:
            with open(self.base_path + self.c_summary_dict_name) as json_file:
                self.c_idx2summary = json.load(json_file)
        except Exception as err:
            logger.exception('Could not open the file %s' %self.c_summary_dict_name)
            raise

        # Fix - Parse keys as ints
        self.c_token2idx = {key: int(value) for key, value in self.c_token2idx.items()}
        self.c_idx2token = {value: key for key, value in self.c_token2idx.items()}
        self.c_idx2summary = {int(key): value for key, value in self.c_idx2summary.items()}
        self.c_summary2idx = {w: i for i, w in self.c_idx2summary.items()}

        try:
            self.c_transformer_model = tf.keras.models.load_model(self.base_path + self.c_transformer_name,
                                                                  custom_objects={'Transformer':Transformer, 'Encoder': Encoder, 'Decoder': Decoder, 'MultiHeadAttention': MultiHeadAttention},
                                                                  compile=False)
        except Exception as err:
            logger.exception('Could not load the model %s' %self.c_transformer_name)
            raise
        
        self.max_len_code = MAX_LENGTH_CODE
        self.max_len_summary = MAX_LENGTH_SUMMARY



    def predict(self, filename, collection, key):
        """
        Predicts NL description for the specified input.

        Parameters
        ----------
        filename: String
            Name of the source code file with extension. Admitted extensions: .java, .c or .cpp
        collection: String
            Collection name where to find the source code file inside the PKM

        Returns
        ----------
        artefact_id: String
            New name for the annotated file (with description) stored in the PKM
        prediction: String
            Natural language description for the source code file filename
        """
        # URL encode the filename and the collection
        start = time.time()
        filename_encoded = requests.utils.quote(filename, safe='')
        collection_encoded = requests.utils.quote(collection, safe='')
        
        # Query the PKM and load the reply
        get_file_url = self.raw_pkm_url + collection_encoded + '/' + filename_encoded
        json_reply = self.call_pkm(get_file_url, 'get', key)

        # Store PKM code attributes
        rel_path = json_reply['rel_path']
        raw_source_code = json_reply['content']
        file_format = json_reply['format']
        encoding = json_reply['encoding']
        file_type = json_reply['type']
        mime_type = json_reply['mime_type']
        
        # Processing pipeline
        extension = filename.split('.')[-1].lower()
        if extension == 'java':
            try:
                # Query the PKM for extracting the methods and load the reply
                java_sourcecode_url = self.java_source_pkm_url + collection_encoded + '/' + filename_encoded
                ast_dicts = self.call_pkm(java_sourcecode_url, 'get', key)
                filtered_ast_dicts = filter_ast_dicts(ast_dicts, [rel_path])
                method_codes, method_line_numbers = split_methods_java(filtered_ast_dicts, [raw_source_code])

            except Exception as err:
                logger.warning('Error in parsing the reply from endpoint %s. Analyzing the entire file ...' %java_sourcecode_url)
                method_codes = [raw_source_code]
                method_line_numbers = [[0]]

            try:
                token_list = tokenization_java(method_codes)
            except Exception as err:
                logger.exception('Error during the tokenization of the source code files')
                abort(500)

            # Vectorize and padding
            X = feature_extraction_inference(self.java_token2idx,
                                             token_list,
                                             self.max_len_code)

            # Predict
            predictions = self.decode_sequence_batch(X,
                                                    self.java_transformer_model,
                                                    self.java_summary2idx,
                                                    self.java_idx2summary,
                                                    self.java_token2idx,
                                                    self.java_idx2token)

        elif extension in ('c', 'h', 'cpp', 'hpp'):
            # try:
            # Query the PKM for extracting the methods and load the reply
            try:
                if extension in ('c', 'h'):
                    sourcecode_url = self.c_source_pkm_url + collection_encoded + '/' + filename_encoded
                    ast_dicts = self.call_pkm(sourcecode_url, 'get', key)
                    filtered_ast_dicts = filter_ast_dicts(ast_dicts, [rel_path])
                    method_codes, method_line_numbers = split_methods_c(filtered_ast_dicts, [raw_source_code])
                else:
                    sourcecode_url = self.cpp_source_pkm_url + collection_encoded + '/' + filename_encoded
                    ast_dicts = self.call_pkm(sourcecode_url, 'get', key)
                    filtered_ast_dicts = filter_ast_dicts(ast_dicts, [rel_path])
                    method_codes, method_line_numbers = split_methods_cpp(filtered_ast_dicts, [raw_source_code])
            except Exception as err:
                logger.warning('Error in parsing the reply from endpoint %s. Analyzing the entire file ...' %sourcecode_url)
                method_codes = [raw_source_code]
                method_line_numbers = [[0]]

            try:
                token_list = tokenization_c(method_codes)
            except Exception as err:
                logger.exception('Error during the tokenization of the source code files')
                abort(500)

            # Vectorize and padding
            X = feature_extraction_inference(self.c_token2idx,
                                             token_list,
                                             self.max_len_code)

            # Predict
            predictions = self.decode_sequence_batch(X,
                                                    self.c_transformer_model,
                                                    self.c_summary2idx,
                                                    self.c_idx2summary,
                                                    self.c_token2idx,
                                                    self.c_idx2token)
        else:
            logger.error('Unknown programming language. The tool is able to process only .java, .cpp and .c files')
            abort(500)

        # Restore original file including comments in a single string
        content, _ = add_prediction_comments([raw_source_code], method_line_numbers, predictions)

        # Save the results in the PKM
        insert_file_url = self.raw_pkm_url + collection_encoded
        try:
            payload = [{'format': file_format, 'encoding': encoding, 'type': file_type, 'rel_path': rel_path, 'content': content[0], 'mime_type': mime_type}]
        except: # Avoid nulls
            payload = [{'format': 'text', 'encoding': 'utf-8', 'type': 'Code', 'rel_path': rel_path, 'content': content[0], 'mime_type': 'plain/text'}]
        
        self.call_pkm(insert_file_url, 'put', key, payload)
            
        return rel_path, predictions



    def predict_all(self, collection, key):
        """
        Predicts NL description for all source code files inside the specified input collection.

        Parameters
        ----------
        collection: String
            Collection name where to find the source code files inside the PKM

        Returns
        ----------
        artefact_ids: String
            Name of source code files annotated and stored in the PKM
        predictions: String
            Natural language descriptions for the source code files
        """
        # URL encode the collection
        collection_encoded = requests.utils.quote(collection, safe='')
        
        # Query the PKM and load the reply
        get_collection_url = self.raw_pkm_url + collection_encoded
        json_reply = self.call_pkm(get_collection_url, 'get', key)

        # Store PKM code attributes
        filenames = []
        raw_source_codes = []
        file_format = []
        encoding = []
        file_type = []
        mime_type = []

        for index in range(len(json_reply)):
            json_reply[index].keys()
            filenames.append(json_reply[index]['rel_path'])
            raw_source_codes.append(json_reply[index]['content'])
            file_format.append(json_reply[index]['format'])
            encoding.append(json_reply[index]['encoding'])
            file_type.append(json_reply[index]['type'])
            mime_type.append(json_reply[index]['mime_type'])

        # check if files in the project are java, c or cpp
        java_files =  [file for file in filenames if file.lower().endswith('.java')]
        c_files =  [file for file in filenames if file.lower().endswith('.c') or file.lower().endswith('.h')]
        cpp_files =  [file for file in filenames if file.lower().endswith('.cpp') or file.lower().endswith('.hpp')]
        c_family_files = c_files + cpp_files

        relevant_files =  java_files + c_files + cpp_files
        predictions = []
        method_line_numbers = []

        if not relevant_files:
            logger.error('No relevant file found. The tool is able to process only .java, .cpp and .c files.')
            abort(500)

        # Processing pipeline
        if java_files:
            try:

                method_codes = []

                # Query the PKM for extracting the methods and load the reply
                java_sourcecode_url = self.java_source_pkm_url + collection_encoded
                ast_dicts = self.call_pkm(java_sourcecode_url, 'get', key)
                filtered_ast_dicts = filter_ast_dicts(ast_dicts, java_files)
                java_method_codes, java_method_line_numbers = split_methods_java(filtered_ast_dicts, raw_source_codes)
                method_codes += java_method_codes
                method_line_numbers += java_method_line_numbers

            except Exception as err:
                logger.warning('Error in parsing the reply from endpoint %s. Analyzing the entire file ...' %java_sourcecode_url)
                java_file_indexes = [filenames.index(f) for f in java_files]
                java_raw_source_codes = [raw_source_codes[f_idx] for f_idx in java_file_indexes]
                method_codes = java_raw_source_codes
                method_line_numbers += [[0]] * len(java_raw_source_codes)

            try:
                token_list = tokenization_java(method_codes)
            except Exception as err:
                logger.exception('Error during the tokenization of the source code files')
                abort(500)

            # Vectorize and padding
            X = feature_extraction_inference(self.java_token2idx,
                                             token_list,
                                             self.max_len_code)

            # Predict
            predictions += self.decode_sequence_batch(X,
                                                     self.java_transformer_model,
                                                     self.java_summary2idx,
                                                     self.java_idx2summary,
                                                     self.java_token2idx,
                                                     self.java_idx2token)

        if c_family_files:

            try:

                method_codes = []

                if c_files:
                    c_sourcecode_url = self.c_source_pkm_url + collection_encoded
                    ast_dicts = self.call_pkm(c_sourcecode_url, 'get', key)
                    filtered_ast_dicts = filter_ast_dicts(ast_dicts, c_files)
                    c_file_indexes = [filenames.index(f) for f in c_files]
                    c_raw_source_codes = [raw_source_codes[f_idx] for f_idx in c_file_indexes]
                    c_method_codes, c_method_line_numbers =  split_methods_c(filtered_ast_dicts, c_raw_source_codes)
                    method_codes += c_method_codes
                    method_line_numbers += c_method_line_numbers
                if cpp_files:
                    cpp_sourcecode_url = self.cpp_source_pkm_url + collection_encoded
                    ast_dicts = self.call_pkm(cpp_sourcecode_url, 'get', key)
                    filtered_ast_dicts = filter_ast_dicts(ast_dicts, cpp_files)
                    cpp_file_indexes = [filenames.index(f) for f in cpp_files]
                    cpp_raw_source_codes = [raw_source_codes[f_idx] for f_idx in cpp_file_indexes]
                    cpp_method_codes, cpp_method_line_numbers =  split_methods_cpp(filtered_ast_dicts, cpp_raw_source_codes)
                    method_codes += cpp_method_codes
                    method_line_numbers += cpp_method_line_numbers

            except Exception as err:
                logger.warning('Error in parsing the reply from endpoint %s. Analyzing the entire file ...')
                c_family_file_indexes = [filenames.index(f) for f in c_family_files]
                c_family_raw_source_codes = [raw_source_codes[f_idx] for f_idx in c_family_file_indexes]
                method_codes = c_family_raw_source_codes
                method_line_numbers += [[0]] * len(c_family_raw_source_codes)

            try:
                token_list = tokenization_c(method_codes)
            except Exception as err:
                logger.exception('Error during the tokenization of the source code files')
                abort(500)

            # Vectorize and padding
            X = feature_extraction_inference(self.c_token2idx,
                                             token_list,
                                             self.max_len_code)

            # Predict
            predictions += self.decode_sequence_batch(X,
                                                     self.c_transformer_model,
                                                     self.c_summary2idx,
                                                     self.c_idx2summary,
                                                     self.c_token2idx,
                                                     self.c_idx2token)

        # Restore original file including comments in a single string
        relevant_file_indexes = [filenames.index(f) for f in relevant_files]
        relevant_source_codes = [raw_source_codes[f_idx] for f_idx in relevant_file_indexes]
        modified_relevant_source_codes, predictions_per_file = add_prediction_comments(relevant_source_codes, method_line_numbers, predictions)

        # Save the results in the PKM
        insert_file_url = self.raw_pkm_url + collection_encoded
        payload = []
        for index, modified_source_code in zip(relevant_file_indexes, modified_relevant_source_codes):
            try:
                payload.append({'format': file_format[index], 'encoding': encoding[index], 'type': file_type[index], 'rel_path': filenames[index], 'content': modified_source_code, 'mime_type': mime_type[index]})
            except: # Avoid nulls
                payload.append({'format': 'text', 'encoding': 'utf-8', 'type': 'Code', 'rel_path': filenames[index], 'content': modified_source_code, 'mime_type': 'plain/text'})

        self.call_pkm(insert_file_url, 'put', key, payload)
        
        return relevant_files, predictions_per_file


    def decode_sequence_batch(self, X, model, target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index):
        """
        Function that generates descriptions in natural language given the codified input sequences.

        Parameters
        ----------
        X: numpy array
            Arrays with the padded input sequences of shape (num_observations, max_sequence_length)
        model: Keras Model
            Model containing the transformer in the encoder-decoder architecture
        target_word_index: Dictionary
            Dictionary for transforming words to ids at the prediction output, containing the words as keys and the ids as values
        reverse_target_word_index: Dictionary
            Dictionary for transforming ids to words at the prediction output, containing the words as keys and the ids as values

        Returns
        ----------
        decoded_sentences: List of strings
            Predictions as natural language descriptions for the different observations passed as the input_seq
        """
        decoded_sentences = []
        for index in range(len(X)):
            sentence = summarize(seq2text(X[index], reverse_source_word_index),
                                 model,
                                 token2idx,
                                 target_word_index,
                                 self.max_len_code,
                                 self.max_len_summary,
                                 reverse_target_word_index)

            decoded_sentences.append(sentence)

        return decoded_sentences

    def call_pkm(self, url, method, key, payload=None):
        """
        Function that calls the API for the PKM for CRUD operations.

        Parameters
        ----------
        url: String
            URL for the endpoint
        method: String
            Method for the query (for the moment only get and put are implemented)
        payload: List of dictionaries
            Payload object for the insertions in the PKM database

        Returns
        ----------
        json_reply: Dictionary
            JSON with the reply from the PKM (only for the get method)
        """
        start = time.time()
        headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'key': key}
        
        if method.lower() == 'get':
            logger.info('Retrieving file from URL %s...' %(url))
            try:
                r = requests.get(url, headers=headers, verify=self.base_path+'pkm.crt')
                json_reply = check_reply(r, method.lower())
            except requests.exceptions.HTTPError as err:
                logger.exception(err)
                raise
            end = time.time()
            logger.info('Download time: %s seconds\n' %(end-start))
            return json_reply

        elif method.lower() == 'put':
            logger.info('Inserting file in URL %s...' %(url))
            try:
                # Insert (if not yet present) or update (if present) source code file into the PKM 
                r = requests.put(url, json=payload, headers=headers, verify=self.base_path+'pkm.crt')
                check_reply(r,  method.lower())
            except requests.exceptions.HTTPError as err:
                logger.exception(err)
                raise
            end = time.time()
            logger.info('Total time for insertion: %s seconds\n' %(end-start))

        else:
            logger.error('Unknown method "%s" for calling the PKM' %method)
            abort(500)
