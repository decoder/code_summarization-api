import os
import re
import pygments
from pygments.lexers import JavaLexer
import nltk
nltk.download('punkt')
import time
import json
import tensorflow as tf

##from https://github.com/wasiahmad/NeuralCodeSum ('A Transformer-based Approach for Source Code Summarization')
from .tokenizer import CodeTokenizer     

def tokenize_files(list_methods):

    lexer = JavaLexer()   
    list_tokens = []
    for snippet in list_methods:
        result = pygments.lex(snippet, lexer)
        list_tokens.append(list(result))

    return list_tokens
    

def split_identifier_names(list_tokens):  # split by snake case, camel case, etc...
    
    #define code tokenizer
    code_tokenizer = CodeTokenizer()
    
    final_token_list = []

    for code_file in list_tokens:
        inside = []
        for tup in code_file:

            #splitting comment and literals with word tokenization + identifiers splitting
            if str(tup[0]).startswith('Token.Comment') or str(tup[0]).startswith('Token.Literal'):

                if len(list(nltk.word_tokenize(tup[1]))) >1:

                    for split in list(nltk.word_tokenize(tup[1])):
                        splitted = ((tup[0], re.sub(r'\W+', '', split)))

                        if len(list(code_tokenizer.tokenize(splitted[1])))>1:
                            for split in list(code_tokenizer.tokenize(splitted[1])):
                                splitted = ((tup[0], split))
                                inside.append(splitted)
                        else:
                            inside.append(splitted)    

            #apply identifiers splitting to names             
            elif str(tup[0]).startswith('Token.Name'):

                if len(list(code_tokenizer.tokenize(tup[1]))) >1:

                    for split in list(code_tokenizer.tokenize(tup[1])):
                        splitted = ((tup[0], split))
                        inside.append(splitted)
                else:
                    inside.append((tup))
                    
            # Filter blank spaces, carriage returns and puntuations       
            elif str(tup[0])=='Token.Text' or str(tup[1])=='\n' or str(tup[1])=='' or str(tup[0])=='Token.Punctuation':
                continue
                
            else:
                inside.append((tup))

        final_token_list.append(inside)

    return final_token_list


def final_preproc(tuple_tokens):
    list_tokens = []
    for file in tuple_tokens:
        file_tokens = []
        for tup in file:
            # Filtering empty tokens, puntuation
            if str(tup[1]) != '':
                if str(tup[0]) != 'Token.Punctuation':
                    if str(tup[0]) == 'Token.Literal.Number.Integer': # Substitute integer tokens by 'INT'
                        file_tokens.append('INT')
                      
                    elif str(tup[0]) == 'Token.Literal.Number.Float': # Substitute float tokens by 'FLOAT'
                        file_tokens.append('FLOAT')
                        
                    else:
                        file_tokens.append(str(tup[1]).lower())
        #print('\nTokenized file after preprocessing: ', file_tokens)
        list_tokens.append(file_tokens)
    return list_tokens
  

def tokenization_java(list_methods):    
    
    start = time.time()
    print('Starting tokenization of source code files...')
    tuple_tokens = tokenize_files(list_methods)
    tuple_tokens = split_identifier_names(tuple_tokens)
    print('Tokenization ready!')
    print('Tokenization time: %0.4f seconds' %(time.time()-start))
    print()

    start = time.time()
    print('Starting preproc of source code files...')
    list_tokens = final_preproc(tuple_tokens)
    print('Preprocessing ready!')
    print('Preprocessing time: %0.4f seconds' %(time.time()-start))
    print()
    
    
    return list_tokens


def import_dicts(dict_path, token2idx_file, word2idx_file, idx2word_file):
    
    start_dir = os.chdir(dict_path)
    working_dir = os.getcwd()

    # Load input and output dictionaries
    # Input
    #token2idx_file = 'token2idx_java.json'
    with open(token2idx_file) as json_file:
        token2idx = json.load(json_file)

    # Output
    #word2idx_file = 'y2idx_java.json'
    with open(word2idx_file) as json_file:
        target_word_index = json.load(json_file)

    #idx2word_file = 'idx2y_java.json'
    with open(idx2word_file) as json_file:
        reverse_target_word_index = json.load(json_file)

    
    reverse_target_word_index = {int(w):i for w, i in reverse_target_word_index.items()}

    return token2idx, target_word_index, reverse_target_word_index


