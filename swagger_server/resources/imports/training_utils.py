from numpy.random import seed
seed(1)

import numpy as np

import tensorflow as tf
from tensorflow.keras.preprocessing.sequence import pad_sequences

from .transformer_modules import create_padding_mask, create_look_ahead_mask


def feature_extraction_inference(token2idx, list_tokens, max_len_text):
    
    # Vectorize sentences
    X = [[token2idx[t] if t in token2idx else 1 for t in file] for file in list_tokens] #1 is label for 'UNK'
    # Padding
    X = pad_sequences(X, maxlen=max_len_text, padding='post', truncating = 'post', value=token2idx['PAD'])
    return X


def create_masks(inp, tar):
    enc_padding_mask = create_padding_mask(inp)
    dec_padding_mask = create_padding_mask(inp)

    look_ahead_mask = create_look_ahead_mask(tf.shape(tar)[1])
    dec_target_padding_mask = create_padding_mask(tar)
    combined_mask = tf.maximum(dec_target_padding_mask, look_ahead_mask)

    return enc_padding_mask, combined_mask, dec_padding_mask

def evaluate(input_document, model, token2idx, target_word_index, encoder_maxlen, decoder_maxlen):
    
    input_document = [token2idx[str(x)] if x in token2idx.keys() else 1  for x in input_document.split(' ')]
    input_document = tf.keras.preprocessing.sequence.pad_sequences([input_document], maxlen=encoder_maxlen, padding='post', truncating='post')

    encoder_input = tf.expand_dims(input_document[0], 0)

    decoder_input = [target_word_index["<start>"]]
    output = tf.expand_dims(decoder_input, 0)

    for i in range(decoder_maxlen-1): 
        enc_padding_mask, combined_mask, dec_padding_mask = create_masks(encoder_input, output)

        this = [encoder_input, output, enc_padding_mask, combined_mask, dec_padding_mask]

        predictions, attention_weights = model(
                this, False
            )

        predictions = predictions[: ,-1:, :]
        predicted_id = tf.cast(tf.argmax(predictions, axis=-1), tf.int32)

        if predicted_id == target_word_index["<end>"]:
            return tf.squeeze(output, axis=0), attention_weights

        output = tf.concat([output, predicted_id], axis=-1)

    return tf.squeeze(output, axis=0), attention_weights

def summarize(input_document, model, token2idx, target_word_index, encoder_maxlen, decoder_maxlen, reverse_target_word_index):
        # not considering attention weights for now, can be used to plot attention heatmaps in the future
    summarized = evaluate(input_document, model, token2idx, target_word_index, encoder_maxlen, decoder_maxlen)[0]
    summarized = np.expand_dims(summarized[1:], 0)  # not printing <go> token
    return ' '.join([reverse_target_word_index[y] for y in summarized[0]])

def seq2summary(input_seq, target_word_index, reverse_target_word_index):
    newString=''
    for i in input_seq:
        if((i!=0 and i!=target_word_index['<start>']) and i!=target_word_index['<end>']):
            newString=newString+reverse_target_word_index[i]+' '
    return newString

def seq2text(input_seq, reverse_source_word_index):
    newString=''
    for i in input_seq:
        if(i!=0):
            newString=newString+reverse_source_word_index[i]+' '
    return newString

def make_predictions(X, y, model, target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index, encoder_maxlen, decoder_maxlen):

    target = []
    decoded = []

    for index in range(len(X)):
        #print(index)
        #print("Snippet:",seq2text(X[index]))
        #print("Original summary:",seq2summary(y[index]))
        #print('Predicted summary: ',summarize(seq2text(X[index])))
        target.append(seq2summary(y[index],target_word_index, reverse_target_word_index))
        decoded.append(summarize(seq2text(X[index], reverse_source_word_index), model, token2idx, target_word_index, encoder_maxlen, decoder_maxlen, reverse_target_word_index))
        #print('\n')

    return decoded, target
