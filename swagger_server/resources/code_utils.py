import io
from typing import Dict, List, Tuple

import pygments
import logging
import pygments.token
import pygments.lexer
from pygments.formatters import NullFormatter
from pygments.lexers import CLexer, CppLexer, JavaLexer
from typing_extensions import Literal
from itertools import islice


# get core logger
logger = logging.getLogger("DECODER_API.core.model")

# definition of some types
SupportedLanguages = Literal["java", "c", "cpp"]

# some useful global constant variables
LEXER_DICT: Dict[SupportedLanguages, pygments.lexer.Lexer] = {
    "java": JavaLexer(),
    "c": CLexer(),
    "cpp": CppLexer(),
}
FORMATTER = NullFormatter()


def split_methods_java(ast_dicts: List[Dict], raw_source_codes: List[str]):
    """
    Splits Java source code files into different methods based on Javaparser AST.

    Parameters
    ----------
    ast_dicts: List[Dict]
        List of dicts with the Javaparser AST information of each of the files.
    raw_source_codes: List[str]
        List of strings containing the different source code files.

    Returns
    ----------
    method_codes: List[str]
        List of strings with all the methods present in the different source code files
    method_line_numbers: List[List[int]]
        List of list with the line numbers of the start of each the methods.
    """

    check_matching_sizes(ast_dicts, raw_source_codes)

    method_codes: List[str] = []
    method_line_numbers: List[List[int]] = []
    for ast_dict, raw_source_code in zip(ast_dicts, raw_source_codes):
        logger.info("Slicing file %s..." % ast_dict["sourceFile"])
        raw_code_by_lines = raw_source_code.splitlines(keepends=True)

        # this returns a list of declared methods in each file
        method_asts = ast_dict["CompilationUnit"]["TypeDeclaration"][
            "MethodDeclaration"
        ]
        method_codes_in_file = []
        method_line_numbers_in_file: List[int] = []
        num_methods = len(method_asts)
        logger.info("Number of methods in original file: %d" % num_methods)
        if num_methods > 0:

            # extract each method (in reverse order is simpler)
            method_end_line = len(raw_code_by_lines)

            # wrap in a list if not list already
            # important to deal with the case of a single method
            # inside a class such as the main method of an app
            if not isinstance(method_asts, list):
                method_asts = [method_asts]

            for method_ast in method_asts[::-1]:

                # this is the starting line including the javadoc if any
                method_start_line_all = method_ast["loc"]["pos_l"] - 1

                # init start line where name is in the declaration
                method_start_line = method_ast["SimpleName"]["loc"]["pos_l"] - 1
                # check if other elements that are not doc start earlier
                for el_key, el_value in method_ast.items():
                    if el_key in ("loc", "Javadoc"):
                        pass
                    # skip lists (e.g. variable declarations)
                    elif isinstance(el_value, dict):
                        el_start_line = el_value["loc"]["pos_l"] - 1
                        if el_start_line < method_start_line:
                            method_start_line = el_start_line

                # get method code from file
                method_code = "".join(
                    raw_code_by_lines[method_start_line:method_end_line]
                )
                method_codes_in_file.append(method_code)

                # add global start line as line index
                method_line_numbers_in_file.append(method_start_line_all)

                # set start as next end line (comments in between will be ignored later)
                method_end_line = method_start_line_all

            # reverse back to get order
            method_codes += method_codes_in_file[::-1]
            method_line_numbers.append(method_line_numbers_in_file[::-1])
        else:
            logger.warning(
                "File %s has no annotated methods in the PKM, the entire file will be analyzed."
                % ast_dict["sourceFile"]
            )
            method_codes.append(raw_source_code)
            method_line_numbers.append([0])

    # remove comments and spaces after method (only \n added)
    method_codes = [
        clean_after_method_code(method_code, "java") for method_code in method_codes
    ]

    return method_codes, method_line_numbers


def split_methods_c(ast_dicts: List[Dict], raw_source_codes: List[str]):
    """
    Splits C source code files into different methods based on Frama C AST.

    Parameters
    ----------
    ast_dicts: List[Dict]
        List of dicts with the Frama C AST information of each of the files.
    raw_source_codes: List[str]
        List of strings containing the different source code files.

    Returns
    ----------
    method_codes: List[str]
        List of strings with all the methods present in the different source code files
    method_line_numbers: List[List[int]]
        List of list with the line numbers of the start of each the methods.
    """

    check_matching_sizes(ast_dicts, raw_source_codes)

    method_codes: List[str] = []
    method_line_numbers: List[List[int]] = []
    for ast_dict, raw_source_code in zip(ast_dicts, raw_source_codes):

        logger.info("Slicing file %s..." % ast_dict["sourceFile"])

        raw_code_by_lines = raw_source_code.splitlines(keepends=True)
        method_line_numbers.append([])

        for ast_obj in ast_dict["globals"]:

            # check if it is a function
            if "GFun" in ast_obj:

                method_start_line = ast_obj["GFun"]["loc"]["pos_start"]["pos_lnum"] - 1
                method_end_line = ast_obj["GFun"]["loc"]["pos_end"]["pos_lnum"] - 1

                method_code = "".join(
                    raw_code_by_lines[method_start_line : method_end_line + 1]
                )

                method_codes.append(method_code)
                method_line_numbers[-1].append(method_start_line)

    return method_codes, method_line_numbers

def iterate_over_ast_dict(ast_dict, raw_code_by_lines, method_codes, method_sublist_numbers):
    """
    Iterate over AST dict for C++ in a recursive mode.

    Parameters
    ----------
    ast_dict: Dict
        Ast dict with the script parsed by frama-clang
    raw_code_by_lines: List[str]
        List of strings containing the different source code lines.
    method_codes: List[str]
        List with all the method codes
    method_sublist_numbers: List[int]
        List containing all the starting line of methods in the script
    """

    for ast_obj in ast_dict["inner"]:
        # check if it is a function
        if ast_obj["kind"] in (
            "CXXConstructorDecl",
            "CXXDestructorDecl",
            "CXXMethodDecl",
            "FunctionDecl"
        ):
            method_start_line = ast_obj["range"]["begin"]["line"] - 1
            method_end_line = ast_obj["range"]["end"]["line"] - 1

            method_code = "".join(
                raw_code_by_lines[method_start_line : method_end_line + 1]
            )
            #avoiding duplicity
            if method_start_line not in method_sublist_numbers:
                method_codes.append(method_code)
                method_sublist_numbers.append(method_start_line)
        
        #going lower level if there is inner data 
        if ast_obj["kind"] in ('NamespaceDecl','CXXRecordDecl'):
            if ("inner" in ast_obj) and ast_obj['inner']:
                iterate_over_ast_dict(ast_obj, raw_code_by_lines, method_codes, method_sublist_numbers)

def split_methods_cpp(ast_dicts: List[Dict], raw_source_codes: List[str]):
    """
    Splits CPP source code files into different methods based on Frama Clang AST.

    Parameters
    ----------
    ast_dicts: List[Dict]
        List of dicts with the Frama Clang AST information of each of the files.
    raw_source_codes: List[str]
        List of strings containing the different source code files.

    Returns
    ----------
    method_codes: List[str]
        List of strings with all the methods present in the different source code files
    method_line_numbers: List[List[int]]
        List of list with the line numbers of the start of each the methods.
    """

    check_matching_sizes(ast_dicts, raw_source_codes)

    method_codes: List[str] = []
    method_line_numbers: List[List[int]] = []
    print(f"FICHEROS: {len(raw_source_codes)}")
    for ast_dict, raw_source_code in zip(ast_dicts, raw_source_codes):
        logger.info("Slicing file %s..." % ast_dict["sourceFile"])

        raw_code_by_lines = raw_source_code.splitlines(keepends=True)
        method_sublist_numbers = []

        iterate_over_ast_dict(ast_dict, raw_code_by_lines, method_codes, method_sublist_numbers)

        method_line_numbers.append(method_sublist_numbers)
    
    return method_codes, method_line_numbers


def clean_after_method_code(method_code: str, code_language: SupportedLanguages) -> str:
    """Returns a copy of the method without spaces, new lines and comments at the end."""

    # get list of tokens in the code
    original_token_list: List[Tuple[pygments.token.Token, str]] = list(
        pygments.lex(method_code, LEXER_DICT[code_language])
    )

    # iterate in reverse order to find index of end of method
    # that is not a space, whitespace or comment)
    end_of_method = len(original_token_list)
    for token_type, token_str in reversed(original_token_list):

        # remove if is comment
        if token_type in pygments.token.Comment:
            end_of_method -= 1
        # remove if is blank, space, newline or tab (text in general at the end)
        elif token_type in pygments.token.Text:
            end_of_method -= 1
        # otherwise break the loop
        else:
            break

    # trim tokens and add new line at the end
    modified_token_list = original_token_list[:end_of_method]
    modified_token_list.append((pygments.token.Text, "\n"))

    file_like = io.StringIO()
    FORMATTER.format(modified_token_list, file_like)

    # seed and read to get string
    file_like.seek(0)
    modified_code = file_like.read()

    return modified_code


def add_prediction_comments(
    raw_source_codes: List[str],
    method_line_numbers: List[List[int]],
    predictions: List[str],
):
    """
    Function that returns the original source code augmented with method comments

    Parameters
    ----------
    raw_source_codes: List[str]
        List of strings containing the different source code files.
    method_line_numbers: List[List[int]]
        List of list with the line numbers of the start of each the methods.
    predictions: List
        List of strings with the predictions (method natural language descriptions)

    Returns
    ----------
    modified_raw_source_codes: List[str]
        List of strings containing the different source code files modified.
    predictions_per_file: List[List[str]]
        List of lists strings with the descriptions. The first element in the outside list contains all the descriptions for the methods present in the first source code file and so on.
    """

    # reshape prediction so that it can match the shape of the method_line_numbers
    num_methods_per_file = [
        len(line_numbers_per_file) for line_numbers_per_file in method_line_numbers
    ]
    # retrieve the number of methods per source code file
    prediction_iter = iter(predictions)
    predictions_per_file = [
        list(islice(prediction_iter, elem)) for elem in num_methods_per_file
    ]

    # iterate for each file
    modified_raw_source_codes: List[str] = []
    for raw_source_code, file_method_line_numbers, file_predictions in zip(
        raw_source_codes, method_line_numbers, predictions_per_file
    ):

        # empty string to fill with modified file
        modified_raw_source_code: str = ""
        for line_number, line_content in enumerate(
            raw_source_code.splitlines(keepends=True)
        ):

            # if line is start of method, add description
            if line_number in file_method_line_numbers:
                method_index = file_method_line_numbers.index(line_number)
                method_description = file_predictions[method_index]
                modified_raw_source_code += add_documentation(method_description)

            modified_raw_source_code += line_content

        modified_raw_source_codes.append(modified_raw_source_code)

    return modified_raw_source_codes, predictions_per_file


def add_documentation(desc):
    annotated_description = "/**\n"
    annotated_description += " * " + 'DECODER-CODE-SUMMARIZATION: ' + desc + "\n"
    annotated_description += " */\n"

    return annotated_description


def check_matching_sizes(ast_dicts, raw_source_codes):

    if len(ast_dicts) != len(raw_source_codes):
        logger.info(
            "Number of files in ast collection: %d",
            len(ast_dicts),
        )
        logger.info(
            "Number of files in RawSourcecode collection: %d", len(raw_source_codes)
        )
        raise Exception(
            "Number of files mismatch between RawSourcecode and ast collections"
        )


def filter_ast_dicts(ast_dicts: List[Dict], filenames: List[str]):

    filtered_ast_dicts = []
    for filename in filenames:
        for ast_dict in ast_dicts:
            if filename == ast_dict["sourceFile"]:
                filtered_ast_dicts.append(ast_dict)

    return filtered_ast_dicts
