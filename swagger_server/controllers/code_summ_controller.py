# -*- coding: utf-8 -*-

import logging
import time
from datetime import datetime as dt
from flask import make_response, jsonify, abort
import connexion

from swagger_server.resources.model import Model
from ..resources.parse_logs import update_invocation, upload_logs
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501


logger = logging.getLogger("DECODER_API.controller")
# Load the model
logger.info("Loading Deep Learning models...")
code_summ_model = Model()


def custom_error(message, status_code):
    return make_response(jsonify(message), status_code)


def create_invocation_results(collection_id, log_message, raw_artifacts=None):

    invocation_results = []
    if raw_artifacts is None:
        raw_artifacts = []

    if log_message:
        # get log id
        log_id = log_message[0]
        invocation_results = [{"path": f"/log/{collection_id}/{log_id}", "type": "log"}]

    for raw_artifact in raw_artifacts:
        invocation_results.append(
            {"path": f"/code/rawsourcecode/{raw_artifact}", "type": "code"}
        )

    return invocation_results


def app_predictions_code_summ(
    artefact_id, collection_id, invocation_id=None
):  # noqa: E501
    """Predicts natural language description given a source code file

     # noqa: E501

    :param artefact_id:
    :type artefact_id: str
    :param collection_id:
    :type collection_id: str

    :rtype: InlineResponse200
    """
    initial_time = dt.now()

    try:
        key = connexion.request.headers["key"]
        if key is None:
            abort(400)

        logger.info(
            "Starting prediction for file %s inside collection %s"
            % (artefact_id, collection_id)
        )
        start = time.time()
        new_artefact_id, prediction = code_summ_model.predict(
            artefact_id, collection_id, key
        )
        end = time.time()
        logger.info(
            "Time for predicting file %s within collection %s: %0.4f seconds"
            % (artefact_id, collection_id, (end - start))
        )

        response = (
            InlineResponse200(artefact_id=new_artefact_id, nl_description=prediction),
            200,
        )
        logger.debug("Reply to the call: %s", response)

        # store the logs
        status, log_message = upload_logs(initial_time, collection_id, key)
        if invocation_id:
            # create invocation results
            invocation_results = create_invocation_results(
                collection_id, log_message, [new_artefact_id]
            )
            # update invocation
            update_invocation(
                collection=collection_id,
                key=key,
                invocation_id=invocation_id,
                status=status,
                invocation_results=invocation_results,
            )

    except Exception as err:
        logger.exception(err)

        # store the logs
        status, log_message = upload_logs(initial_time, collection_id, key)
        if invocation_id:
            # create invocation results
            invocation_results = create_invocation_results(collection_id, log_message)
            # update invocation
            update_invocation(
                collection=collection_id,
                key=key,
                invocation_id=invocation_id,
                status=status,
                invocation_results=invocation_results,
            )

        raise

    return response


def app_predictions_code_summ_all(collection_id, invocation_id=None):  # noqa: E501
    """Predicts natural language description given a collection of source code files

     # noqa: E501

    :param collection_id:
    :type collection_id: str

    :rtype: List[InlineResponse200]
    """
    initial_time = dt.now()

    try:
        key = connexion.request.headers["key"]
        if key is None:
            abort(400)

        logger.info(
            "Starting prediction for all files inside collection %s" % collection_id
        )
        start = time.time()
        artefact_ids, predictions = code_summ_model.predict_all(collection_id, key)
        end = time.time()
        logger.info(
            "Time for predicting all files within collection %s: %0.4f seconds"
            % (collection_id, (end - start))
        )

        response = []
        for index in range(len(artefact_ids)):
            resp = InlineResponse200(
                artefact_id=artefact_ids[index], nl_description=predictions[index]
            )
            response.append(resp)
        response = response, 200
        logger.debug("Reply to the call: %s", response)

        # store the logs
        status, log_message = upload_logs(initial_time, collection_id, key)
        if invocation_id:
            # create invocation results
            invocation_results = create_invocation_results(
                collection_id, log_message, artefact_ids
            )
            # update invocation
            update_invocation(
                collection=collection_id,
                key=key,
                invocation_id=invocation_id,
                status=status,
                invocation_results=invocation_results,
            )

    except Exception as err:
        logger.exception(err)

        # store the logs
        status, log_message = upload_logs(initial_time, collection_id, key)
        if invocation_id:
            # create invocation results
            invocation_results = create_invocation_results(collection_id, log_message)
            # update invocation
            update_invocation(
                collection=collection_id,
                key=key,
                invocation_id=invocation_id,
                status=status,
                invocation_results=invocation_results,
            )

        raise

    return response
