connexion == 2.7.0
python_dateutil == 2.6.0
setuptools >= 21.0.0
numpy==1.19.2
tensorflow==2.4.1
Pygments==2.5.2
nltk==3.4.1
Flask == 2.0.1
swagger-ui-bundle == 0.0.8
python-dotenv
