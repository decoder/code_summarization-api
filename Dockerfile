FROM python:3.7-slim-buster

# Create directories suited to the application
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy and install requirements
COPY requirements.txt /usr/src/app/
RUN pip3 install --no-cache-dir -r requirements.txt

# Install documentation for swagger
RUN pip3 install connexion[swagger-ui]

# Copy contents from local directory to docker container
COPY . /usr/src/app

# Open the port where the API will be available from compose args
ARG PORT 
EXPOSE ${PORT}

ENTRYPOINT ["python3"]
CMD ["-m", "swagger_server"]
